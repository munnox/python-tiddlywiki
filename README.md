# Tiddlywiki Python Library

A api attachment to the standard Tiddly wiki server.

Allows the ability to:

* Search using filters.
* Get a full tiddler by name.
* Save a full tidder defined by a yaml file. With an optional merge (Experimental).
* Delete a tiddler by title.

## To install

```bash
poetry add git+https://gitlab.com/munnox/tiddlywiki-python.git#main
```

Installing and allowing it to be editable

```bash
poetry add --editable git+ssh://git@gitlab.com:munnox/tiddlywiki-python.git#main
poetry add --editable git+https://gitlab.com/munnox/tiddlywiki-python.git#main
```

## To build and deploy

```bash
poetry build
twine upload -r local dist/*
```

## Running the cli

```bash
tw_get --help
tw_search --help
tw_save --help
tw_delete --help

poetry shell
# Experimental group cli
twcli --help
twcli search --help
twcli get --help
twcli save --help
twcli delete --help
```

## Running tests an coverage

Test all

```bash
pytest
```

Testing Coverage

```bash
pytest --cov --cov-report=html
```

```bash
pre-commit run -a
```

```bash
pytest --snapshot-update
```

## Using the library and the client

```python
import tiddlywiki

tiddler = tiddlerwiki.Tiddler.from_tw_file("tiddler.tid")
tiddler = tiddlerwiki.Tiddler.from_tw_file("tiddler.md", "tiddler.md.meta")

tid = tiddlerwiki.Tiddler("basic tiddler", text="Main text for tiddler")

tw_url = "http://localhost:5000"
auth = None
# auth = (tw_username, tw_password)
twclient = tiddlywiki.twclient.TWClient(tw_url, auth=auth)

# Search example
tiddlers = tw_client.search("[all[tiddlers]!is[system]sort[title]]")
# Get example
tiddler = tw_client.get("test/#TestNode")
# Delete example
# tw_client.delete(tiddler.title)
# Save example
# tw_client.save(tid)
```
