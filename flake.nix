{
  description = "Tiddlywiki intereface application packaged using poetry2nix originaly from gitlab:munnox/explore-nix";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    # nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    official-templates.url = github:NixOS/templates;
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix, official-templates }:
    {
      templates = {
        copy = {
          path = ./.;
          description = "Use this folder as a template";
        };
      } // official-templates.templates;
      checks.x86_64-linux.math = with nixpkgs.legacyPackages.x86_64-linux;
        runCommand "tw-test" {} ''
          pytest
          touch $out
        '';
    } //
    flake-utils.lib.eachDefaultSystem (system:
      let
        # see https://github.com/nix-community/poetry2nix/tree/master#api for more functions and examples.
        pkgs = nixpkgs.legacyPackages.${system};
        newpkgs = self.packages.${system};
        inherit (poetry2nix.lib.mkPoetry2Nix { inherit pkgs; }) mkPoetryApplication;
      in
      {
        packages = {
          twcli = mkPoetryApplication { projectDir = self; };
          hello = pkgs.hello;
          env = pkgs.buildEnv {
            name="combined-env";
            paths = [
              newpkgs.twcli
            ];
          };
          # Used via nix shell .#combine
          # path command NOT available on nix develop .# combine
          combine = pkgs.symlinkJoin {
            name = "combine";
            paths = [
              newpkgs.twcli
            ];
            # The post build doesn't work or run visibily
            preBuild = ''
              echo App Dev Combined
            '';
          };
          # Run the text under nix run .#sh
          sh = pkgs.writeShellApplication {
            name = "combineshell";
            runtimeInputs = [
              newpkgs.twcli
              # pkgs.ansible
              # pkgs.hello
              # pkgs.tree
            ];
            # This works and the language seems to be set before running ansible
            text = ''
              echo TW Dev Combined "''${@}"
              export LANG=C.UTF-8
              export LC_ALL=C.UTF-8
              twcli ''${@}
            '';
          };
          # default = newpkgs.twcli;
          # To run with nix develop .#dev
          dev = 
            let
              test = "simple";
              debugtrace = x: pkgs.lib.traceSeq x x;
              r = debugtrace test; #newpkgs.twcli;
              t = builtins.trace newpkgs.twcli;
            in pkgs.mkShell {
            nativeBuildInputs = [
              # pkgs.python311
              # newpkgs.twcli
              newpkgs.env
            ];
            # buildInputs = [
            #   # pkgs.python311
            #   pkgs.ansible
            #   # pkgs.poetry
            #   # newpkgs.twcli
            # ];
            # inputsFrom = [ newpkgs.twcli ];
            packages = newpkgs.twcli.nativeBuildInputs ++ [
              # pkgs.python311
              # pkgs.poetry
              # pkgs.ansible
              # newpkgs.twcli
            ];
            shellHook = ''
              # echo "hello world"
              # export LANG=C.UTF-8
              # export LC_ALL=C.UTF-8
              export PS1="NIX-$PS1"
              # Debugging and exploring derivations and their attributes
            '';
          };

          fallback = pkgs.mkShell {
            # packages = [ poetry2nix.packages.${system}.poetry ];
            # inputsFrom = [ newpkgs.dev ];
            packages = [
              pkgs.poetry
              # newpkgs.twcli
            ];
          };

          develop = pkgs.mkShell {
            # packages = [ poetry2nix.packages.${system}.poetry ];
            # inputsFrom = [ newpkgs.dev ];
            packages = [
              newpkgs.twcli
            ];
          };
          # Used by running nix build .#docker
          # loaded by running ./result | docker load
          # started by running docker run -it --rm <result of load>
          docker = pkgs.dockerTools.streamLayeredImage {
            name = "tiddlywiki";
            contents = [ newpkgs.twcli ];
            config.Cmd = [ "tw_cli" ];
          };
        };

        # devShells.dev =  newpkgs.dev;
        devShells.default = newpkgs.dev;

        # templates = {
        #   python-local = {
        #     path = ./.;
        #     description = "This Repo to be used as a template";
        #   };

        # };
        # // official-templates.templates;
      });
}
