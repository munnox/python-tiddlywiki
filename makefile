
# Needs to be within the env
export_pip:
	# poetry config warnings.export false
	poetry run poetry export --without-hashes --format=requirements.txt > requirements.txt

dev:
	poetry run poetry shell

test:
	poetry run pytest