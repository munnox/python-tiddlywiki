"""
Library to test the tiddlewiki api client

Author: Robert Munnoch
"""

import logging
import os

import pytest
from dotenv import load_dotenv

import tiddlywiki
import tiddlywiki.twclient
from tests.testdata import TIDDLERFILELIST, TIDDLERLIST
from tiddlywiki.errors import TWError
from tiddlywiki.tw import Tiddler

logger = logging.getLogger(__name__)

load_dotenv()

tw_url = os.getenv("TW_URL", "http://localhost:5000")
tw_username = os.getenv("TW_USERNAME", "admin")
tw_password = os.getenv("TW_PASSWORD", "pass")
tw_verify = False

@pytest.fixture
def tw_client():
    """Fixture to make a Tiddlywiki client."""
    twclient = tiddlywiki.twclient.TWClient(tw_url, auth=(tw_username, tw_password), verify=tw_verify)
    # twclient.save(Tiddler("#TestNode"))
    return twclient


@pytest.fixture
def tw_client_with_to():
    """Fixture to make a Tiddlywiki client."""
    twclient = tiddlywiki.twclient.TWClient(
        tw_url, auth=(tw_username, tw_password), timeout=0.1, verify=tw_verify
    )
    # twclient.save(Tiddler("#TestNode"))
    return twclient


@pytest.fixture
def tw_client_bad_auth():
    """Fixture to make a Tiddlywiki client."""
    return tiddlywiki.twclient.TWClient(
        tw_url, auth=("NotRealUsername", "CrazyPassword"), verify=tw_verify
    )


def test_tw_search_get_save_delete_bad_auth(tw_client_bad_auth):
    """Testing the tw client search and get tiddlers."""

    with pytest.raises(TWError, match=r".*Unauthorised.*"):
        tw_client_bad_auth.search("[all[tiddlers]!tag[system]sort[title]]")

    with pytest.raises(TWError, match=r".*Unauthorised.*"):
        tw_client_bad_auth.get("titles")

    with pytest.raises(TWError, match=r".*Unauthorised.*"):
        tid = Tiddler("test")
        tw_client_bad_auth.save(tid)

    with pytest.raises(TWError, match=r".*Unauthorised.*"):
        tid = Tiddler("test")
        tw_client_bad_auth.delete("titles")


def test_tw_search_get(tw_client):
    """Testing the tw client search and get tiddlers."""

    tiddlers = tw_client.search(filter="[all[tiddlers]!is[system]sort[title]]")

    # logger.debug(tiddlers)
    logger.debug(len(tiddlers))
    logger.debug(tiddlers[0])
    assert len(tiddlers) >= 1

    # Test with normal client but with a timeout on the request
    tiddlers = tw_client.search(
        filter="[all[tiddlers]!is[system]sort[title]]", timeout=1
    )

    # logger.debug(tiddlers)
    logger.debug(len(tiddlers))
    logger.debug(tiddlers[0])
    assert len(tiddlers) >= 1


def test_tw_search_get_with_to(tw_client_with_to):
    """Testing the tw client search and get tiddlers."""

    tiddlers = tw_client_with_to.search(filter="[all[tiddlers]!is[system]sort[title]]")

    # logger.debug(tiddlers)
    logger.debug(len(tiddlers))
    logger.debug(tiddlers[0])
    assert len(tiddlers) >= 1


def test_tw_filter_get_with_to(tw_client):
    """Testing the tw client search and get tiddlers."""

    # with pytest.raises(
    #     TWError, match=r".*Route not found requires a filter route in the server.*"
    # ):
    results = tw_client.install_filter_route()
    logger.debug(results)

    # NOTE: Need to reboot NodeJS tiddlywiki servera to allow the route to be incorperated

    results = tw_client.filter(filter="[all[tiddlers]!is[system]sort[title]]")

    logger.debug(results)


def test_tw_get(tw_client):
    """Testing the tw client get save and delete tiddlers."""
    # Get a tiddler from the nth search tiddler by title
    tiddler = tw_client.get("test/#TestNode")
    logger.info("Tiddler: %s", tiddler)
    assert isinstance(tiddler, Tiddler)


def ensure_clear_tiddler(tw_client, tiddler):
    """Ensure the tiddlers are cleared from wiki."""
    # Try ot get a tiddler that isn't there
    local_tiddler = tw_client.get(tiddler.title)
    # logger.info("tiddler (None): %s", tiddler)
    # if tidder is there delete
    if local_tiddler is not None:
        tw_client.delete(tiddler.title)
    # Test the tiddler presence
    local_tiddler = tw_client.get(tiddler.title)
    if local_tiddler is None:
        return True
    return False


def check_tiddler(base_tiddler: Tiddler, tiddler_to_check: Tiddler):
    """Check and compare two tiddlers."""
    assert isinstance(base_tiddler, Tiddler)
    assert isinstance(tiddler_to_check, Tiddler)
    assert base_tiddler.title == tiddler_to_check.title
    # assert tiddler.type == tid_to_save.type
    assert base_tiddler.text == tiddler_to_check.text
    assert base_tiddler.tags == tiddler_to_check.tags
    # assert base_tiddler.fields == tiddler_to_check.fields
    logger.info("Base: %s", base_tiddler.fields)
    logger.info("Check: %s", tiddler_to_check.fields)
    assert len(base_tiddler.fields) == len(tiddler_to_check.fields)


def test_tw_get_save_delete(tw_client):
    """Testing the tw client get save and delete tiddlers."""

    for tid in TIDDLERFILELIST:
        tw_client.save(tid)
    tids = TIDDLERLIST

    assert all((ensure_clear_tiddler(tw_client, tid) for tid in tids))
    # assert False

    # Save the test tiddler
    assert all((tw_client.save(tid) for tid in tids))
    # assert False

    # Recover the tiddler saved
    for tid in tids:
        tiddler = tw_client.get(tid.title)
        logger.info(
            "tiddler recovered:\n%s\n%s\n%s",
            tiddler.pretty_dump(),
            tiddler.to_dict(),
            tiddler.to_tw_dict(),
        )

        check_tiddler(tid, tiddler)

    # assert tw_client.delete(tid_to_save.title)
    assert all((ensure_clear_tiddler(tw_client, tid) for tid in tids))

    # tiddler = tw_client.get(tid_to_save.title)
    # logger.info("tiddler (None): %s", tiddler)
    # assert tiddler is None
    # assert False
