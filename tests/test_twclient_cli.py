"""
Library to test the tiddlewiki api client

Author: Robert Munnoch
"""

import json
import logging
import os

from click.testing import CliRunner
from dotenv import load_dotenv

from tests.testdata import YAMLTESTFILE
from tiddlywiki.cli import cli, get, save, search

logger = logging.getLogger(__name__)

load_dotenv()

tw_url = os.getenv("TW_URL", "https://localhost:8443")
tw_username = os.getenv("TW_USERNAME", "admin")
tw_password = os.getenv("TW_PASSWORD", "pass")


# @pytest.fixture
# def tw_client_cli():
#     """Fixture to make a Tiddlywiki client."""
#     runner = CliRunner()
#     result = runner.invoke(hello, ["Peter"])
#     assert result.exit_code == 0
#     assert result.output == "Hello Peter!\n"
#     return tiddlywiki.twclient.TWClient(tw_url, auth=(tw_username, tw_password))


def test_basic_cli():
    """Testing the tw cli get save and delete tiddlers."""
    runner = CliRunner()
    result = runner.invoke(get, ["Peter"])
    # print(result.stdout)
    assert result.stdout.find("Got unexpected extra argument (Peter)") >= 0
    assert result.exit_code == 2

    with open("tests/testfile.yaml", "w", encoding="utf-8") as file_handle:
        file_handle.write(YAMLTESTFILE)

    result = runner.invoke(
        cli, ["--no-verify", "-vv", "save", "--tiddler", "./tests/testfile.yaml", "--update-modified-date"]
    )
    print("stdout", result.stdout)
    # assert result.stdout.find("Got unexpected extra argument (Peter)") >= 0
    assert result.exit_code == 0

    # def test_basic_cli_get():
    #     """Testing the tw cli get save and delete tiddlers."""
    # runner = CliRunner()
    result = runner.invoke(cli, ["--no-verify", "-v", "get", "--title", "test/#TestNode"])
    # print(result.stdout)
    # assert result.stdout.find("Got unexpected extra argument (Peter)") >= 0
    assert result.exit_code == 0

    result = runner.invoke(cli, ["--no-verify", "get", "--title", "test/#TestNode"])
    try:
        tiddlers = json.loads(result.stdout)
    except Exception as error:
        raise Exception(f"Test json failed: {error} stdout: {result.stdout}") from error
    assert isinstance(tiddlers, list)
    assert len(tiddlers) == 1
    assert tiddlers[0]["title"] == "test/#TestNode"
    # assert result.stdout.find("Got unexpected extra argument (Peter)") >= 0
    assert result.exit_code == 0

    result = runner.invoke(
        cli, ["--no-verify", "search", "--filter", "[all[tiddlers]!is[system]sort[title]]"]
    )
    try:
        tiddlers = json.loads(result.stdout)
    except Exception as error:
        raise Exception(f"Test json failed: {error} stdout: {result.stdout}") from error
    assert isinstance(tiddlers, list)
    assert len(tiddlers) >= 1
    # assert tiddlers[0]["title"] == "noderedtest"
    # assert result.stdout.find("Got unexpected extra argument (Peter)") >= 0
    assert result.exit_code == 0

    # result = runner.invoke(
    #     search,
    #     ["-v"],
    #     env={"TW_FILTER": "test"},  # "[all[tiddlers]!tag[system]sort[title]]"}
    # )  # , ["--filter", ""])
    # print(result.stdout)
    # assert False
    # tiddlers = json.loads(result.stdout)
    # assert isinstance(tiddlers, list)
    # assert len(tiddlers) == 1
    # assert tiddlers[0]["title"] == "noderedtest"
    # # assert result.stdout.find("Got unexpected extra argument (Peter)") >= 0
    # assert result.exit_code == 0
