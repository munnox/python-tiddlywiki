"""
Library to test the tiddlewiki api client

Author: Robert Munnoch
"""

import logging
from datetime import datetime
from textwrap import dedent

from tiddlywiki.tw import Tiddler

logger = logging.getLogger(__name__)

YAMLTESTFILE = """
- title: "test/#TestNode"
  type: text/vnd.tiddlywiki
  bag: default
  created: "20140508151449207"
  modified: "20140608151449207"
  tags:
  - NodeRED/Flow
  - Tag with space
  - Tidderwiki
  - Tiddlywiki/API
  - Python/Library/Tiddlywiki/API
  text: |
    Testing tiddler creation

    On mulitple lines in a YAML
  extra_field: "testing fields"
"""


def correct(text: str) -> str:
    """Correct a multiline block of text to strip and dedent the block."""
    return dedent(text.strip("\n"))


TIDDLERLIST = [
    Tiddler(title="tw_python_#api/test"),
    Tiddler(title="tw_python_api/test1", fields={"test": "foo", "more": "bar"}),
]

TIDDLERFILELIST = [
    Tiddler(
        "local/test/#TestNodeFile",
        text=correct(
            """
            Normal tiddlywiki text tiddler

            @@background-color: lightgray;
            <ul>
            <$list filter="[tag[tiddler/testing]]">
            <li> <<currentTiddler>> </li>
            </$list>
            </ul>
            @@

            test further
            <<now "DD">>
        """
        ),
        tags=["first tag wih space", "tiddler/testing"],
        created=datetime(2021, 11, 20, 12, 48, 00, 34),
        modified=datetime(2021, 11, 21, 12, 48, 00, 34),
    ),
    Tiddler(
        "local/test/#TestNode_md",
        type="text/x-markdown",
        text=correct(
            """
            Normal markdown text tiddler

            # Heading

            ## 2nd level heading
        """
        ),
        tags=["first tag wih space", "tiddler/testing"],
        created=datetime(2021, 11, 20, 12, 48, 00, 34),
        modified=datetime(2021, 11, 21, 12, 48, 00, 34),
    ),
    Tiddler(
        "local/test/#TestNode.md",
        type="text/x-markdown",
        text="Normal tiddlywiki text tiddler",
        tags=["first tag wih space", "tiddler/testing"],
        created=datetime(2021, 11, 20, 12, 48, 00, 34),
        modified=datetime(2021, 11, 21, 12, 48, 00, 34),
    ),
    Tiddler(
        "local/test/#TestNode.html",
        type="text/x-markdown",
        text="Normal tiddlywiki text tiddler",
        tags=["first tag wih space", "tiddler/testing"],
        created=datetime(2021, 11, 20, 12, 48, 00, 34),
        modified=datetime(2021, 11, 21, 12, 48, 00, 34),
    ),
    Tiddler(
        "local/test/#TestNode_nonetype",
        type=None,
        text="Normal tiddlywiki text tiddler",
        tags=["first tag wih space", "tiddler/testing"],
        created=datetime(2021, 11, 20, 12, 48, 00, 34),
        modified=datetime(2021, 11, 21, 12, 48, 00, 34),
    ),
    Tiddler(
        "local/test/#TestNode A very very long title to ensure the tiddler "
        "saver uses the first 200 characters as the tiddler wiki javascript "
        "version does so this title need to be super long to be over 200 "
        "characters nonetype",
        type=None,
        text="Normal tiddlywiki text tiddler",
        tags=["first tag wih space", "tiddler/testing"],
        created=datetime(2021, 11, 20, 12, 48, 00, 34),
        modified=datetime(2021, 11, 21, 12, 48, 00, 34),
    ),
    Tiddler(
        "local/test/#TestNode A very very long title to ensure the tiddler "
        "saver uses the first 200 characters as the tiddler wiki javascript "
        "version does so this title need to be super long to be over 200 "
        "characters markdown",
        type="text/x-markdown",
        text="Normal tiddlywiki text tiddler",
        tags=["first tag wih space", "tiddler/testing"],
        created=datetime(2021, 11, 20, 12, 48, 00, 34),
        modified=datetime(2021, 11, 21, 12, 48, 00, 34),
    ),
    Tiddler(
        "local/test/#TestNode_latex",
        type="text/latex",
        text=correct(
            r"""
        \documentclass[10pt,a4paper,draft]{article}

        \author{Robert Munnoch}
        \title{Curriculum Vitae}
        \begin{document}
        \maketitle

        \section{Contact Details}

        Normal Latex tiddler

        \end{document}

        """
        ),
        tags=["first tag wih space", "tiddler/testing"],
        created=datetime(2021, 11, 20, 12, 48, 00, 34),
        modified=datetime(2021, 11, 21, 12, 48, 00, 34),
    ),
    Tiddler(
        "local/test/#TestNode_latex.latex",
        type="text/latex",
        text=correct(
            r"""
        \documentclass[10pt,a4paper,draft]{article}

        \author{Robert Munnoch}
        \title{Curriculum Vitae}
        \begin{document}
        \maketitle

        \section{Contact Details}

        Normal Latex tiddler

        \end{document}

        """
        ),
        tags=["first tag wih space", "tiddler/testing", "latex"],
        created=datetime(2021, 11, 20, 12, 48, 00, 34),
        modified=datetime(2021, 11, 21, 12, 48, 00, 34),
    ),
]
