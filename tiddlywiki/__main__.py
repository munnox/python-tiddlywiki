"""
Library to run the tiddleywiki api client

Author: Robert Munnoch
"""

import logging
import logging.config

logging.captureWarnings(True)

from tiddlywiki.cli import cli

logger = logging.getLogger(__name__)


if __name__ == "__main__":
    # pylint: disable-next=no-value-for-parameter
    cli(obj={})
