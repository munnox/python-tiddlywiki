"""
Library to run the tiddleywiki api client

Author: Robert Munnoch
"""

import json
import logging
import logging.config
import os
import sys
from datetime import datetime
from typing import Any, Callable, Dict, List, Optional

import click

# pylint: disable-next=import-error
import yaml

import tiddlywiki
from tiddlywiki.errors import TWError
from tiddlywiki.tw import Tiddler

# import dotenv
from dotenv import load_dotenv

import urllib3

urllib3.disable_warnings()

logger = logging.getLogger(__name__)


load_dotenv()
tw_url = os.getenv("TW_URL", "http://localhost:5000")
tw_verify = bool(os.getenv("TW_VERIFY", "true") == 'true')
tw_username = os.getenv("TW_USERNAME", "admin")
tw_password = os.getenv("TW_PASSWORD", "password")
tw_filter = os.getenv("TW_FILTER", "[all[tiddlers]!is[system]sort[title]]")
tw_title = os.getenv("TW_TITLE", None)
tw_format = os.getenv("TW_FORMAT", "prettyjson")
tw_file = os.getenv("TW_FILE", None)


def format_output(
    output: List[Tiddler],
    result_format: str = "json",
    count_function: Optional[Callable] = None,
    base_directory=None,
):
    """Format the output variable based on the format request.

    json - outputs objects as utf-8 encoded json
    jsonascii - outputs object as ascii encoded json
    prettyjson - outputs object as pretty utf-8 encoded json
    yaml - outputs objects as utf-8 encoded yaml
    yamlascii - outputs object as ascii encoded yaml
    tid - outputs objects as tiddler files
    """
    if result_format == "json":
        click.echo(json.dumps([obj.to_dict() for obj in output], ensure_ascii=False))
    elif result_format == "jsonascii":
        click.echo(json.dumps([obj.to_dict() for obj in output], ensure_ascii=True))
    elif result_format == "prettyjson":
        click.echo(
            json.dumps([obj.to_dict() for obj in output], indent=2, ensure_ascii=False)
        )
    elif result_format == "yaml":
        click.echo(yaml.dump([obj.to_dict() for obj in output], allow_unicode=True))
    elif result_format == "yamlascii":
        click.echo(yaml.dump([obj.to_dict() for obj in output], allow_unicode=False))
    elif result_format == "tid":
        if base_directory is None:
            base_directory = os.path.abspath("./")
        for out in output:
            # if isinstance(out, Tiddler):
            #     with open(f"{out.title}.tid", "w") as file_handle:
            #         out.to_tw_file(file_handle)
            out.to_tw_file_meta(base_directory)
    elif result_format == "count":
        if callable(count_function):
            click.echo(count_function(output))
        else:
            click.echo(len(output))
    else:
        raise NotImplementedError(f"Format requested ({result_format}) not recognised.")

def create_tw_client(ctx):
    # click.echo(f"Variables: {ctx.obj['tw_url']} {ctx.obj['tw_verify']}")
    try:
        twclient = tiddlywiki.twclient.TWClient(ctx.obj['tw_url'], auth=(ctx.obj['tw_username'], ctx.obj['tw_password']), verify=ctx.obj['tw_verify'])
    except Exception as error:
        click.echo(f"Error Creating the Tiddlywiki Client due to: {error}")
    return twclient

@click.command()
@click.option(
    "-s",
    "--filter",
    "search_filter",
    default=tw_filter,
    help=(
        "Search filter of tiddlers to get. "
        "Can be defined with environment variable TW_FILTER."
    ),
)
@click.pass_context
def search(ctx, search_filter):
    """Tiddlywiki client search tiddlers."""
    result_format = ctx.obj['result_format']
    verbose = ctx.obj['verbosity']
    # if verbose > 0:
    #     clisetup()
    # if verbose:
    #     logger.info("Verbose output enabled, level: %s", verbose)
    # logger.info("Filter: %s", search_filter)
    twclient = ctx.obj['twclient']
    try:
        tiddlers = twclient.search(search_filter)

        format_output(
            tiddlers,
            result_format,
            lambda out: f"{search_filter}={len(out)}",
        )
    except TWError as error:
        click.echo(f"Error encountered while searching tiddlers: {error}")
        sys.exit(1)


@click.command()
@click.option(
    "-s",
    "--filter",
    "search_filter",
    default=tw_filter,
    help=(
        "Runs a given filter agiast the tiddlywiki service. "
        "Can be defined with environment variable TW_FILTER."
    ),
)
@click.pass_context
def filter(ctx, search_filter):
    """Tiddlywiki client filter tiddlers by tw filter query."""
    result_format = ctx.obj['result_format']
    verbose = ctx.obj['verbosity']
    if verbose:
        logger.info("Verbose output enabled, level: %s", verbose)
    # logger.info("Filter: %s", search_filter)
    twclient = ctx.obj['twclient']
    try:
        # Runs the filter and returns the raw json result
        output = twclient.filter(search_filter)

        if result_format == "json":
            click.echo(json.dumps(output, ensure_ascii=False))
        elif result_format == "jsonascii":
            click.echo(json.dumps(output, ensure_ascii=True))
        elif result_format == "prettyjson":
            click.echo(json.dumps(output, indent=2, ensure_ascii=False))
        elif result_format == "yaml":
            click.echo(yaml.dump(output, allow_unicode=True))
        elif result_format == "yamlascii":
            click.echo(yaml.dump(output, allow_unicode=False))
        elif result_format == "count":
            click.echo(len(output))
        else:
            raise NotImplementedError(
                f"Format requested ({result_format}) not recognised."
            )
    except TWError as error:
        click.echo(f"Error encountered while searching tiddlers: {error}")
        sys.exit(1)


@click.command()
@click.option(
    "--title",
    default=tw_title,
    help="Title of tiddler to get. Can be defined with environment varible TW_TITLE.",
)
@click.option(
    "-d",
    "--dir",
    "base_directory",
    default="./",
    help="Base Directory got the tiddler file output",
)
@click.pass_context
def get(ctx, title, base_directory):
    """Tiddlywiki client get tiddlers by title."""
    result_format = ctx.obj['result_format']
    verbose = ctx.obj['verbosity']
    # if verbose:
    #     logger.info("Verbose output enabled, level: %s", verbose)
    twclient = ctx.obj['twclient']
    try:
        if title is None:
            raise TWError("Env variable TW_TITLE or --title must be defined.")
        tiddler = twclient.get(title)

        if tiddler is None:
            if verbose:
                click.echo(f"No tiddler found with title {title}")
            tiddlers = []
        else:
            tiddlers = [tiddler]

        format_output(
            tiddlers, result_format, base_directory=os.path.abspath(base_directory)
        )
    except TWError as error:
        click.echo(f"Error encountered while getting tiddler: {error}")
        sys.exit(1)


@click.command()
@click.option(
    "--tiddler",
    default=None,
    help=(
        "Path to a tiddler file to save uses extenstion to determine type. "
        "Can be defined with environment varible tw_tiddler."
    ),
)
@click.option(
    "--merge",
    default=False,
    help="Flag to use the tiddler file to merge into any tiddler already in the wiki.",
)
@click.option(
    "--update-modified-date",
    default=True,
    is_flag=True,
    help=(
        "Flag to tell the cli to update the modified date before saving. "
        "Can be defined with environment varible tw_tiddler."
    ),
)
@click.pass_context
def save(ctx, tiddler, merge, update_modified_date):
    """Tiddlywiki client saving tiddlers from files."""
    # pylint: disable=too-many-locals,too-many-branches
    result_format = ctx.obj['result_format']
    verbose = ctx.obj['verbosity']
    # if verbose > 0:
    #     clisetup()
    if verbose:
        logger.info(
            "Update modified date: %s",
            update_modified_date,
        )
    twclient = ctx.obj['twclient']
    try:
        if tiddler is None:
            raise TWError(
                "A tiddler file path must be defined with with --tiddler or TW_TIDDLER."
            )
        tiddler_path = os.path.abspath(tiddler)
        tiddler_file_exist = os.path.exists(tiddler_path)
        logger.info(
            "Tiddler path: %s  exists: %s",
            tiddler_path,
            tiddler_file_exist,
        )
        ext = os.path.splitext(tiddler_path)[1]
        if ext in [".yml", ".yaml"]:
            with open(tiddler_path, "r", encoding="utf-8") as file_handle:
                tiddlerlist = yaml.safe_load(file_handle) #, Loader=yaml.Loader)
        elif ext in [".json"]:
            with open(tiddler_path, "r", encoding="utf-8") as file_handle:
                tiddlerlist = json.load(file_handle) #, Loader=yaml.Loader)
        else:
            raise TWError(f"Wrong file type. {ext}")
        if verbose:
            logger.info("Tiddler list count to save: %s", len(tiddlerlist))
        result_tiddlers = []
        for tiddlerdict in tiddlerlist:
            tiddler_to_save = Tiddler.from_dict(tiddlerdict)
            final_tiddler = tiddler_to_save
            if merge:
                tiddler_from_wiki = twclient.get(tiddler_to_save.title)
                # Check to see if a tiddler has been found else abandon the merge.
                if tiddler_from_wiki is not None:
                    final_tiddler = Tiddler.merge(tiddler_from_wiki, tiddler_to_save)

            if verbose:
                logger.debug("Tiddler to save:\n%s", final_tiddler)

            if update_modified_date:
                logger.info("Updating the modified date stamp.")
                final_tiddler.modified = datetime.now()
            else:
                logger.info("Using the Tiddler raw.")

            receipt = twclient.save(final_tiddler)
            result_tiddlers.append(receipt)
            logger.info("Saved tiddler returning results")

        if not all((r.status for r in result_tiddlers)):
            if verbose:
                click.echo(f"Tiddlers were not able to be saved\n{result_tiddlers}")
            sys.exit(3)
        else:
            if verbose:
                click.echo("Results:")
        format_output(result_tiddlers, result_format)

        if verbose:
            click.echo(f"Save Complete")
        sys.exit(0)
    except TWError as error:
        click.echo(f"TWError encountered while saving tiddler: {error}")
        sys.exit(1)
    except Exception as error:
        click.echo(f"Error '{type(error)}' encountered while saving tiddler: {error}")
        raise error
        # sys.exit(2)


@click.command()
@click.option(
    "--title",
    default=tw_title,
    help="Title of tiddler to delete. Can be defined with environment varible TW_TITLE.",
)
@click.pass_context
def delete(ctx, title):
    """Tiddlywiki client delete tiddlers."""
    result_format = ctx.obj['result_format']
    # verbose = ctx.obj['verbosity']
    # if verbose:
    #     logger.info("Verbose output enabled, level: %s", verbose)
    twclient = ctx.obj['twclient']
    try:
        if title is None:
            raise TWError("Env variable TW_TITLE or --title must be defined.")
        tiddler = twclient.get(title)

        if tiddler is None:
            click.echo(f"No tiddler found with title {title}")
            sys.exit(1)

        removalresult = twclient.delete(tiddler.title)

        format_output([tiddler.to_dict(), removalresult.to_dict()], result_format)
    except TWError as error:
        click.echo(f"Error encountered while deleting tiddler: {error}")
        sys.exit(1)


@click.group()
# @click.option('--debug/--no-debug', default=False,
#     help=(
#         "Flag to set Debug mode"
#     )
# )
@click.option('--verify/--no-verify', default=tw_verify,
    help=(
        "Flag to verify the cerificates. "
        "Can be defined with environment variable TW_VERIFY."
    )
)
@click.option(
    "-u",
    "--url",
    "tw_url",
    default=tw_url,
    help=(
        "Tiddlywiki URL to contact. "
        "Can be defined with environment variable TW_URL."
    ),
)
@click.option(
    "--username",
    "tw_username",
    default=tw_username,
    help=(
        "Tiddlywiki Username. "
        "Can be defined with environment variable TW_USERNAME."
    ),
)
@click.option(
    "--password",
    "tw_password",
    default=tw_password,
    help=(
        "Tiddlywiki Username. "
        "Can be defined with environment variable TW_PASSWORD."
    ),
)
@click.option(
    "-f",
    "--format",
    "result_format",
    default=tw_format,
    help="Result output format. Can be defined with environment variable TW_FORMAT.",
)
@click.option(
    "-v",
    "--verbose",
    "verbosity",
    default=False,
    count=True,
    help="Enable logging output.",
)
@click.pass_context
def cli(ctx, verify, tw_url, tw_username, tw_password, result_format, verbosity):
    ctx.ensure_object(dict)

    # ctx.obj['DEBUG'] = debug
    ctx.obj['tw_url'] = tw_url
    ctx.obj['tw_username'] = tw_username
    ctx.obj['tw_password'] = tw_password
    ctx.obj['tw_verify'] = verify
    ctx.obj['result_format'] = result_format
    ctx.obj['verbosity'] = verbosity

    if verbosity > 0:
        clisetup()
    if verbosity:
        logger.info("Verbose output enabled, level: %s", verbosity)
    twclient = create_tw_client(ctx) 
    
    ctx.obj['twclient'] = twclient


cli.add_command(search, "search")
cli.add_command(filter, "filter")
cli.add_command(get, "get")
cli.add_command(save, "save")
cli.add_command(delete, "delete")

@cli.command()
@click.pass_context
def info(ctx):
    """Display the current information being used by twcli."""
    click.echo(f"Info for the TW CLI Environment:")
    # click.echo(f"Debug mode is {'on' if ctx.obj['DEBUG'] else 'off'}")
    click.echo(f"Tiddlywiki URL is {ctx.obj['tw_url']}")
    click.echo(f"Tiddlywiki Username is {ctx.obj['tw_username']}")
    click.echo(f"Tiddlywiki Password is {ctx.obj['tw_password']}")
    click.echo(f"Tiddlywiki verify certificates is {ctx.obj['tw_verify']}")
    click.echo(f"Tiddlywiki Result Format is {ctx.obj['result_format']}")
    click.echo(f"Tiddlywiki Verbosity is {ctx.obj['verbosity']}")
    

class ConfigError(Exception):
    """Config Error Exception."""


def getconfig(filepath) -> Optional[Dict[str, Any]]:
    """Loads a JSON file specified by the `filepath` and returns the dict"""
    settings = None
    if os.path.exists(filepath):
        try:
            logger.info("Picked up a config yaml file")
            with open(filepath, "r", encoding="utf-8") as file_handle:
                settings = yaml.safe_load(file_handle) #, Loader=yaml.Loader)
                # settings = json.load(fh)
            logger.debug("Settings from File: %s are %s", filepath, settings)
        except Exception as error:
            msg = f"Setting loading error: {type(error)}, message: {error}"
            logger.error(msg)
            raise ConfigError(msg) from error
    return settings


if __name__ == "__main__":
    # pylint: disable-next=no-value-for-parameter
    cli(obj={})

LOGSTR = """
---
logging:
    version: 1
    disable_existing_loggers: false
    formatters:
        compactinfo:
            format: '%(asctime)s|%(levelname)s|%(name)s|%(funcName)s:%(lineno)d|>%(message)s'
        xml:
            format: '<%(levelname)s time="%(asctime)s" name="%(name)s" where="%(pathname)s:%(lineno)d" funcname="%(funcName)s">%(message)s</%(levelname)s>'
    handlers:
        console:
            class: logging.StreamHandler
            formatter: compactinfo
            level: DEBUG
            stream: ext://sys.stdout
    loggers:
        __main__:
            level: DEBUG
            propagate: 'no'
    root:
        handlers:
        - console
        level: DEBUG
"""


def clisetup(make_config: bool = False) -> Optional[Dict[str, Any]]:
    """CLI Setup with config and logging."""
    configpath = os.getenv("TW_CONFIG", "config.yaml")
    config = getconfig(configpath)
    if config is None:
        config = yaml.safe_load(LOGSTR)
    # If asked write the default logging config to a file
    if make_config:
        with open(configpath, "w", encoding="utf-8") as file_handle:
            yaml.dump(config, file_handle, Dumper=yaml.Dumper)
    logging.config.dictConfig(config["logging"])
    logger.info("Config:\n%s", yaml.dump(config, Dumper=yaml.Dumper))
    return config
