"""
Libary to hole errors raised by library

Author: Robert Munnoch
"""


class TWError(Exception):
    """Raised when critical error foound in the interface."""

    # pass
