"""
Library to interface with a Tiddlywiki server system.

Author: Robert Munnoch
"""

import logging
from enum import Enum
from typing import Any, Dict

from .tw import Tiddler

logger = logging.getLogger(__name__)


class ReciptType(Enum):
    """Enum for receipt types."""

    SAVE: str = "save"
    DELETE: str = "delete"


class Receipt:
    """Receipt class to hold the context around an operation."""

    status: bool
    type: ReciptType

    def __init__(self, status, type):
        self.status = status
        self.type = type

    def to_dict(self) -> Dict[str, Any]:
        """Convert the Receipt instance into a dict for serialisation."""
        receipt_dict = {
            "status": self.status,
            "type": self.type.value,
        }
        return receipt_dict


class SaveReceipt(Receipt):
    """Receipt class to hold the context around a save operation."""

    etag: str
    tiddler: Tiddler
    type: ReciptType = ReciptType.SAVE

    def __init__(self, status, etag, tiddler):
        super().__init__(status=status, type=ReciptType.SAVE)
        self.etag = etag
        self.tiddler = tiddler

    def to_dict(self) -> Dict[str, Any]:
        """Convert the Receipt instance into a dict for serialisation."""
        receipt_dict = super().to_dict()
        receipt_dict.update(
            {
                "etag": self.etag,
                "tiddler": self.tiddler.to_dict(),
            }
        )
        return receipt_dict


class DeleteReceipt(Receipt):
    """Receipt class to hold the context around a delete operation."""

    title: str
    type: ReciptType = ReciptType.DELETE

    def __init__(self, status, title):
        super().__init__(status=status, type=ReciptType.DELETE)
        self.title = title

    def to_dict(self) -> Dict[str, Any]:
        """Convert the Receipt instance into a dict for serialisation."""
        receipt_dict = super().to_dict()
        receipt_dict.update(
            {
                "title": self.title,
            }
        )
        return receipt_dict
