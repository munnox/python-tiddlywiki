"""
Libary to interface with a Tiddlywiki server system.

Author: Robert Munnoch
"""

import logging
import os
from datetime import datetime, timedelta

import yaml

from tests.testdata import TIDDLERFILELIST

from .tw import Tiddler

logger = logging.getLogger(__name__)


def test_tiddler_object_class(snapshot):
    """Testing the Tiddler class."""
    # Get a tiddler from the nth search tiddler by title
    collection = []
    for tiddler in TIDDLERFILELIST:
        tw_dict = tiddler.to_dict()
        tw_tw_dict = tiddler.to_tw_dict()
        collection.append({"to_dict": tw_dict, "to_tw_dict": tw_tw_dict})
        logger.info("Tiddler: %s", tw_dict)
        assert "title" in tw_tw_dict
        # assert "fields" not in tw_tw_dict
        assert "original" not in tw_tw_dict
        logger.info("Tiddler tw: %s", tw_tw_dict)
        assert isinstance(tiddler, Tiddler)
    snapshot.assert_match(yaml.dump(collection), "tiddlers_dict_output.yml")

    # assert False


def test_tiddler_merge(snapshot):
    """Testing the tw client get save and delete tiddlers."""
    # Get a tiddler from the nth search tiddler by title
    date = Tiddler.process_string_to_date("20211105130901142")
    tiddler_a = Tiddler(
        "testing",
        text="original text",
        fields={
            "testreplacewithb": "test from a",
            "testkeyboth": "same in both",
            "onlyinA": "value_in_A",
        },
        tags=["tag with space", "tag/nospace", "tag to remove"],
        created=date,
        modified=date,
    )
    createdtimediff = timedelta(seconds=2)
    modifiedtimediff = timedelta(seconds=8)
    tiddler_b = Tiddler(
        "testingnew",
        fields={
            "testkeyboth": "same in both",
            "testreplacewithb": "test from b",
            "extrafield": "extravalue",
        },
        tags=["tag with space", "tag/nospace", "newtag", "--tag to remove"],
        created=tiddler_a.created + createdtimediff,
        modified=tiddler_a.modified + modifiedtimediff,
    )
    logger.info("Tiddler A:\n%s", tiddler_a.pretty_dump())
    logger.info("Tiddler B:\n%s", tiddler_b.pretty_dump())
    merged = Tiddler.merge(tiddler_a, tiddler_b)
    # logger.info("tiddler merge result:\n%s", merged.pretty_dump())
    # logger.info("tiddler merge diff:\n%s", "\n".join(Tiddler.diff(tiddler_a, tiddler_b)))
    logger.info(
        "tiddler Tidder A to merge diff:\n%s",
        "\n".join(Tiddler.diff(tiddler_a, merged)),
    )
    snapshot.assert_match(yaml.dump(merged.to_dict()), "merged_tiddler.yml")
    assert merged.title == "testingnew"
    assert merged.text == "original text"
    assert merged.fields == {
        "onlyinA": "value_in_A",
        "testkeyboth": "same in both",
        "testreplacewithb": "test from b",
        "extrafield": "extravalue",
    }
    assert set(merged.tags) == set(["tag with space", "tag/nospace", "newtag"])
    assert Tiddler.process_date_to_string(
        merged.created
    ) == Tiddler.process_date_to_string(tiddler_b.created)
    assert Tiddler.process_date_to_string(
        merged.modified
    ) == Tiddler.process_date_to_string(tiddler_b.modified)
    # assert False


def test_tw_dates():
    """Exercising the tiddly date methods for repeatability."""
    orgdatetime = datetime.now()
    date_str = Tiddler.process_date_to_string(orgdatetime)
    logger.info("String datetime: %s %s", date_str, len(date_str))
    date_obj = Tiddler.process_string_to_date(date_str)
    logger.info("parsed date: %s %s", date_obj.second, orgdatetime.second)
    assert len(date_str) == 17
    assert date_obj.year == orgdatetime.year
    assert date_obj.month == orgdatetime.month
    assert date_obj.day == orgdatetime.day
    assert date_obj.hour == orgdatetime.hour
    assert date_obj.minute == orgdatetime.minute
    assert date_obj.second == orgdatetime.second


def test_tw_tags():
    """Exercising the tiddly tags methods for repeatability."""
    tag_string = (
        "[[spacy tags]] Tidderwiki Tiddlywiki/API NodeRED/Flow [[Tag with space]]"
    )
    tag_list = Tiddler.process_tag_string_to_list(tag_string)
    logger.info("String datetime: %s %s", tag_list, len(tag_list))
    assert len(tag_list) == 5
    new_tag_string = Tiddler.process_tag_list_to_string(tag_list)
    logger.info("tag string: %s", new_tag_string)
    assert tag_string == new_tag_string


def test_tw_tid_files_names(snapshot):
    """Excercising the tiddler local file storage adapter."""

    collected = []

    for tid in TIDDLERFILELIST:
        files = tid.get_filesafe_titles()
        collected.append({"title": tid.title, "type": tid.type, "files": files})

    snapshot.assert_match(yaml.dump(collected), "tiddlers_file_output.yml")


def test_tw_tid_files():
    """Excercising the tiddler local file storage adapter."""
    basepath = os.path.abspath("./")

    for tid in TIDDLERFILELIST:
        files = tid.get_filesafe_titles()
        # for file in files:
        #     file.find("")
        tid.to_tw_file_meta(basepath)
        # Check the tiddler file created
        for file in files:
            filepath = os.path.join(basepath, file)
            assert os.path.exists(filepath)
            logger.info("File path: %s", filepath)
            with open(filepath, "r", encoding="utf-8") as file_handle:
                data = file_handle.read()
            logger.info("Data: %s", data)
            if len(files) == 1:
                assert data.find(tid.title) > 0
                assert data.find(tid.text) > 0
            elif file.endswith(".meta"):
                assert data.find(tid.text) < 0
            else:
                assert data.find(tid.text) == 0
        # Create
        pathfiles = [os.path.join(basepath, f) for f in files]
        if len(pathfiles) == 1:
            loaded_tiddler = Tiddler.from_tw_file(pathfiles[0])
        elif len(pathfiles) == 2:
            loaded_tiddler = Tiddler.from_tw_file(pathfiles[0], pathfiles[1])

        assert loaded_tiddler.title == tid.title
        assert loaded_tiddler.type == tid.type
        assert loaded_tiddler.tags == tid.tags
        assert loaded_tiddler.text == tid.text

        tid.delete_tw_file_meta(basepath)
