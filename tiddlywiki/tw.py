"""
Libary to interface with a Tiddlywiki server system.

Author: Robert Munnoch
"""

import base64
import dataclasses
import difflib
import io
import json
import logging
import os
import pprint
import re
from dataclasses import dataclass, field
from datetime import datetime
from typing import IO, Any, Dict, List, Optional, Type, TypeVar

from .errors import TWError

logger = logging.getLogger(__name__)

TiddlerType = TypeVar("TiddlerType", bound="Tiddler")
system_fields = [
    "title",
    "type",
    "bag",
    "revision",
    "tags",
    "text",
    "created",
    "creator",
    "modified",
    "modifier",
]


@dataclass
# pylint: disable-next=too-many-instance-attributes
class Tiddler:
    """Tiddler Class for Schema Checking and Processing."""

    title: str
    type: Optional[str] = field(default="text/vnd.tiddlywiki")
    bag: str = field(default="default")
    revision: int = field(default=0)
    text: Optional[str] = field(default=None)
    tags: List[str] = field(default_factory=list)
    created: datetime = field(default_factory=datetime.now)
    creator: Optional[str] = field(default=None)
    modified: datetime = field(default_factory=datetime.now)
    modifier: Optional[str] = field(default=None)
    fields: Dict[str, Any] = field(default_factory=dict)
    original: Optional[Dict[str, Any]] = field(
        default=None,
    )

    def to_dict(self) -> Dict[str, Any]:
        """Convert a Tiddler to a dict object."""
        try:
            tid_dict = dataclasses.asdict(self)
            # tid_dict["title"] = tid_dict["title"].encode("utf-8")

            # tid_dict["tags"] = sorted(tid_dict["tags"])
            tid_dict["created"] = Tiddler.process_date_to_string(tid_dict["created"])
            tid_dict["modified"] = Tiddler.process_date_to_string(tid_dict["modified"])
            # Remove the orignal data from a dict dump
            # NOTE Can remove the original key to save the data transfer
            del tid_dict["original"]
            return tid_dict
        except Exception as error:
            raise TWError(
                f"Tidder to convert to dict. Error: {error} Tiddler: {tid_dict}"
            ) from error

    def to_tw_dict(self) -> Dict[str, Any]:
        """Convert a Tiddler to a tiddlywiki compatable dict object."""
        try:
            tid_dict = dataclasses.asdict(self)

            tid_dict["tags"] = Tiddler.process_tag_list_to_string(tid_dict["tags"])
            tid_dict["created"] = Tiddler.process_date_to_string(tid_dict["created"])
            tid_dict["modified"] = Tiddler.process_date_to_string(tid_dict["modified"])

            # Remove the orignal data from a dict dump
            # NOTE Can remove the original key to save the data transfer
            del tid_dict["original"]
            return tid_dict
        except Exception as error:
            raise TWError(
                f"Tidder to convert to TW dict. Error: {error} Tiddler: {tid_dict}"
            ) from error

    def to_tw_file(self, file_handle: IO, extra="all") -> None:
        """Convert a Tiddler to a tiddlywiki compatible file object."""
        try:
            tid_dict = self.to_tw_dict()

            # Remove the orignal data from a dict dump
            # NOTE Can remove the original key to save the data transfer
            # del tid_dict["original"]
            if extra in ["meta", "all"]:
                for k in tid_dict["fields"]:
                    file_handle.write(f"{k}: {tid_dict['fields'][k]}\n")

                del tid_dict["fields"]

                for k in tid_dict:
                    if k in ["text"]:
                        continue
                    file_handle.write(f"{k}: {tid_dict[k]}\n")

            if extra in ["all"]:
                file_handle.write("\n")
            if extra in ["text", "all"]:
                file_handle.write(f"{tid_dict['text']}")

        except Exception as error:
            raise TWError(
                f"Tidder to convert to TW dict. Error: {error} Tiddler: {tid_dict}"
            ) from error

    def get_filesafe_titles(self):
        """Make and return filename for the tiddler."""
        # From the tiddly wiki generators
        # https://github.com/Jermolene/TiddlyWiki5/blob/master/plugins/tiddlywiki/filesystem/filesystemadaptor.js
        # $:/core/modules/commands/savewikifolder.js
        # $:/core/modules/utils/filesystem.js
        # $:/plugins/tiddlywiki/filesystem/filesystemadaptor.js
        regex = re.compile(r"[/\/|]", re.IGNORECASE)
        file_title = regex.sub("_", self.title[:200])
        file_parts = os.path.splitext(file_title)
        if self.type is None:
            exts = [".tid"]
        else:
            try:
                exts = self.fileextmap[self.type]
            except KeyError:
                exts = ["", ".meta"]
        if len(file_parts) == 2:
            # Check to see if the detected ext is the same as the mapping
            if file_parts[1] == exts[0]:

                full_file_titles = [f"{file_parts[0]}{ext}" for ext in exts]
            else:
                full_file_titles = [f"{file_title}{ext}" for ext in exts]

        logging.info(
            "File extension type: %s exts: %s file_parts: %s Tiddler titles: %s",
            self.type,
            exts,
            file_parts,
            full_file_titles,
        )
        return full_file_titles

    def get_text_data(self):
        """Parse tiddler text to byte string."""
        if self.text is not None:
            return base64.b64decode(self.text)
        else:
            return b""

    def get_databytes(self):
        """Get a BytesIO object of the tiddler text field."""
        # Build databytes object.
        try:
            databytes = io.BytesIO(self.get_text_data())
        except Exception as error:
            dat = "(None)"
            if self.text is not None:
                dat = self.text[:10]
            logger.error(
                "Error while making BytesIO Error (%s) %s, Data [:10] or None='%s'",
                type(error),
                error,
                dat,
            )
            raise
        return databytes

    fileextmap = {
        "text/vnd.tiddlywiki": [".tid"],
        "text/plain": [".txt", ".txt.meta"],
        "text/html": [".html", ".html.meta"],
        "appication/javscript": [".js", ".js.meta"],
        "text/css": [".css", ".css.meta"],
        "image/jpeg": [".jpg", ".jpg.meta"],
        "image/png": [".png", ".png.meta"],
        "image/gif": [".gif", ".gif.meta"],
        "image/svg+xml": [".svg", ".svg.meta"],
        "text/x-markdown": [".md", ".md.meta"],
        "text/markdown": [".md", ".md.meta"],
        # "text/x-yaml": ["", ".meta"],
        # "text/yaml": ["", ".meta"],
    }

    def to_tw_file_meta(self, basepath):
        """Convert a Tiddler to a tiddlywiki ccompatible file object on a base path encoded as utf-8."""
        # needs_meta = self.type != "text/vnd.tiddlywiki"
        # exts = self.fileextmap[self.type]
        # logging.info("File extension %s %s", self.type, exts)
        # file_title = self.title.replace("/", "_").replace("\\",)
        full_file_titles = self.get_filesafe_titles()
        full_file_paths = [
            os.path.join(basepath, full_file_title)
            for full_file_title in full_file_titles
        ]
        if len(full_file_paths) == 1:
            with open(full_file_paths[0], "w", encoding="utf-8") as file_handle:
                self.to_tw_file(file_handle, extra="all")
        if len(full_file_paths) == 2:
            with open(full_file_paths[0], "w", encoding="utf-8") as file_handle:
                self.to_tw_file(file_handle, extra="text")
            with open(full_file_paths[1], "w", encoding="utf-8") as file_handle:
                self.to_tw_file(file_handle, extra="meta")

    @classmethod
    def from_tw_file(cls, filepath, metapath=None):
        """Load a Tiddler from a tiddlywiki compatible file object on a base path from utf-8 encoded file."""

        pattern = r"(\w+):[ ]*([\w \.#-\[\]\/]*)"
        regex = re.compile(pattern, re.IGNORECASE)
        # matches = re.finditer(pattern, tagtext)

        tiddler = Tiddler("def")
        try:
            full_file_paths = [f for f in [filepath, metapath] if f is not None]
            if len(full_file_paths) == 1:
                with open(full_file_paths[0], "r", encoding="utf-8") as file_handle:
                    lines = file_handle.readlines()
                    vars, end_of_meta = cls._parse_meta_file(regex, lines)
                    tiddler_fields = cls.convert_vars_to_fields(vars)
                    tiddler = Tiddler.from_dict(tiddler_fields)
                    tiddler.text = "".join(lines[end_of_meta:])

                    logger.debug(
                        "Tiddler:\n%s\nLines:\n%s\n" "vars:\n%s\nLinenumber: %s",
                        tiddler,
                        lines,
                        vars,
                        end_of_meta,
                    )
            if len(full_file_paths) == 2:
                # Parse meta file first to setup tiddler
                with open(full_file_paths[1], "r", encoding="utf-8") as file_handle:
                    metalines = file_handle.readlines()
                    vars, end_of_meta = cls._parse_meta_file(regex, metalines)
                    tiddler_fields = cls.convert_vars_to_fields(vars)
                    tiddler = Tiddler.from_dict(tiddler_fields)
                # Then parse and add tiddler text file to the tiddler
                with open(full_file_paths[0], "r", encoding="utf-8") as file_handle:
                    textlines = file_handle.readlines()
                    tiddler.text = "".join(textlines)
                logger.debug(
                    "Tiddler:\n%s\nText Lines:\n%s\n" "Meta Lines:\n%s",
                    tiddler,
                    textlines,
                    metalines,
                )
            # raise Exception("Fail to debug")

        except Exception as error:
            raise TWError(
                f"Tidder to convert from TW file. Error: {error} Tiddler state: {tiddler}"
            ) from error
        return tiddler

    @classmethod
    def _parse_meta_file(cls, regex, lines):
        """Parse tiddler meta data variables from a set of lines."""
        vars = []
        end_of_meta = 0
        for line_number, line in enumerate(lines):
            # When the parse gets to the first empty line then finsh and take the rest for the text
            if line == "\n":
                end_of_meta = line_number + 1
                break
            for match in regex.finditer(line):
                vars.append(
                    {
                        "id": match.groups()[0],
                        "value": match.groups()[1],
                        "textmatched": match.group(),
                    }
                )

        return vars, end_of_meta

    @classmethod
    def convert_vars_to_fields(cls, vars):
        """Convert parsed and matched vars to tiddler fields."""
        tiddler_fields = {element["id"]: element["value"] for element in vars}
        if tiddler_fields["type"].lower() == "none":
            tiddler_fields["type"] = None
        return tiddler_fields

    def delete_tw_file_meta(self, basepath):
        """Remove the files for this tiddler based on the title.

        Performs no check on the files.
        """
        full_file_titles = self.get_filesafe_titles()
        full_file_paths = [
            os.path.join(basepath, full_file_title)
            for full_file_title in full_file_titles
        ]
        for filepath in full_file_paths:
            os.remove(filepath)

    @classmethod
    def from_dict(cls: Type[TiddlerType], obj: Dict[str, Any]) -> TiddlerType:
        """Create a Tiddler from a dict object."""
        try:

            # logger.info(
            #     "tidder dict in:\n%s",
            #     json.dumps(obj, indent=2),
            # )
            tag_string = obj.get("tags", "")
            if isinstance(tag_string, str):
                tags = Tiddler.process_tag_string_to_list(obj.get("tags", ""))
            elif isinstance(tag_string, list):
                tags = tag_string
            else:
                raise TWError(f"Tiddler tag type not recognised {type(tag_string)}")

            if "fields" not in obj:
                final_fields = {
                    key: obj[key] for key in obj if key not in system_fields
                }
            else:
                final_fields = obj["fields"]
            now_time = Tiddler.process_date_to_string(datetime.now())
            def_tid = Tiddler(title=obj["title"])

            tiddler = cls(
                title=obj["title"],
                type=obj.get("type", def_tid.type),
                revision=obj.get("revision", def_tid.revision),
                tags=tags,
                text=obj.get("text", def_tid.text),
                created=Tiddler.process_string_to_date(obj.get("created", now_time)),
                creator=obj.get("creator", None),
                modified=Tiddler.process_string_to_date(obj.get("modified", now_time)),
                modifier=obj.get("modifier", None),
                fields=final_fields,
                original=obj,
            )
            # logger.info(
            #     "tidder instance:\n%s",
            #     json.dumps(tiddler.to_dict(), indent=2),
            # )
            return tiddler
        except KeyError as error:
            raise TWError(f"Object to convert. Error: {error} Obj: {obj}") from error

    def pretty_dump(self):
        """Create a pretty printed string dump of a Tiddler."""
        tid_dict = {"Tiddler": self.to_dict()}
        return json.dumps(tid_dict, sort_keys=True, indent=2)

    @classmethod
    def merge(
        cls: Type[TiddlerType], tid_a: TiddlerType, tid_b: TiddlerType
    ) -> "Tiddler":
        """Merge two tiddlers into a copy of tiddler A."""
        tid_a_dict = tid_a.to_dict()
        tid_b_dict = tid_b.to_dict()

        newtid = Tiddler.from_dict({**tid_a_dict, **tid_b_dict})

        if tid_b.text is None:
            newtid.text = tid_a.text
        if tid_b.type is None:
            newtid.type = tid_a.type

        def remove_tag(tag):
            return tag.find("--") == 0

        org_tags = [t for t in tid_a.tags if not remove_tag(t)]
        new_tags = [t for t in tid_b.tags if not remove_tag(t)]
        tags_to_remove = [t.strip("-") for t in tid_b.tags if remove_tag(t)]
        # NOTE Fix with more determinisic merge routine
        news_tags = list(set(org_tags) | set(new_tags))
        # logger.info(f"Merge: {newtags}, {tags_to_remove}\n{news_tags}")
        newtid.tags = sorted([t for t in news_tags if t not in tags_to_remove])

        newtid.fields = {**tid_a.fields, **tid_b.fields}

        # merge the orignals if both tiddler have them defined.
        if tid_a.original is not None and tid_b.original is not None:
            newtid.original = {**tid_a.original, **tid_b.original}
        # perserve the orignal from tid_a if tid_b does not have one.
        elif tid_a.original is not None and tid_b.original is None:
            newtid.original = tid_a.original
        # Set the orignal from tid_a as a dict if both tid_a and tid_b does not have one.
        elif tid_a.original is None and tid_b.original is None:
            newtid.original = tid_a.to_dict()
        return newtid

    @classmethod
    def diff(cls: Type[TiddlerType], tid_a, tid_b) -> List[str]:
        """Compute a simple diff between two tiddlers."""
        tid_a_dict = tid_a.to_dict()
        tid_b_dict = tid_b.to_dict()

        # newtid = Tiddler.from_dict({ **tid_a_dict, **tid_b_dict})
        # newtid.tags = list(set(tid_a.tags) | set(tid_b.tags))

        diff = difflib.ndiff(
            pprint.pformat(tid_a_dict).splitlines(),
            pprint.pformat(tid_b_dict).splitlines(),
        )
        return list(diff)

    @staticmethod
    def process_tag_string_to_list(tag_string: str):
        """Convert a tiddlywiki tag string to a list of tags."""
        tagpattern = re.compile(
            r"(?P<grp>(?P<tag>[\w\/]+)|\[\[(?P<tags>[\w \/]+)\]\]) *"
        )
        matches = tagpattern.finditer(tag_string)
        tags = []
        for match in matches:
            groupdict = match.groupdict()
            tag = groupdict["tag"]
            tagspace = groupdict["tags"]
            final_tag = tagspace if tagspace is not None else tag
            tags.append(final_tag)
        return tags

    @staticmethod
    def process_tag_list_to_string(tag_list):
        """Convert a tiddlywiki tag list to a string of tags."""

        def wrap(tag):
            if tag.find(" ") >= 0:
                return f"[[{tag}]]"
            else:
                return tag

        tags = [wrap(tag) for tag in tag_list]
        return " ".join(tags)

    @staticmethod
    def process_date_to_string(date_obj: datetime) -> str:
        """Convert a tiddlywiki date to a string."""
        return date_obj.strftime("%Y%m%d%H%M%S%f")[:-3]

    @staticmethod
    def process_string_to_date(date_str: str) -> datetime:
        """Convert a tiddlywiki string date to a date."""
        return datetime.strptime(date_str + "000", "%Y%m%d%H%M%S%f")
