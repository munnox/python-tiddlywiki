"""
Library to interface with a Tiddlywiki server system.

Author: Robert Munnoch
"""

import logging
import textwrap
from typing import List, Optional, Tuple
from urllib.parse import quote, urlencode, urljoin

from requests import Session
from requests.exceptions import ConnectionError

from .errors import TWError
from .receipts import DeleteReceipt, SaveReceipt
from .tw import Tiddler

logger = logging.getLogger(__name__)


class BasicPrefixSession(Session):  # pylint: disable=too-few-public-methods
    """Basic Session to allow a url prefix to be maintained."""

    def __init__(self, *args, prefix_url=None, timeout=None, verify=True, **kwargs):
        super().__init__(*args, **kwargs)
        self.prefix_url = prefix_url
        self.last_full_url = self.prefix_url
        self.timeout = timeout
        self.verify = verify
        self.auth = kwargs.get("auth", None)

    # pylint: disable=missing-function-docstring
    def request(self, method, url, *args, **kwargs):
        self.last_full_url = urljoin(self.prefix_url, url)
        if (self.auth is not None) and ("auth" not in kwargs):
            kwargs["auth"] = self.auth

        if (self.timeout is not None) and ("timeout" not in kwargs):
            kwargs["timeout"] = self.timeout
        if "verify" not in kwargs:
            kwargs["verify"] = self.verify
        return super().request(method, self.last_full_url, *args, **kwargs)


class TWClient:
    """Main Interface library for the javascript Tiddlywiki server.

    Example
    =======

    >>> tw_url = "http://localhost:5000"
    >>> auth = None
    >>> # auth = (tw_username, tw_password)
    >>> twclient = tiddlywiki.twclient.TWClient(tw_url, auth=auth)
    >>> # Search example
    >>> tiddlers = tw_client.search(filter="[all[tiddlers]!is[system]sort[title]]")
    >>> tiddler = tw_client.get("test/#TestNode")

    """

    _url: str
    _auth: Optional[Tuple[str, str]]

    def __init__(
        self,
        url: str = "http://localhost:5000",
        auth: Optional[Tuple[str, str]] = None,
        **kwargs,
    ):
        """Create a TW client intance to connect to a tiddlywiki server."""
        self._url = url
        if not self._url.endswith("/"):
            self._url += "/"
        self.session = BasicPrefixSession(prefix_url=self._url, **kwargs)
        self._auth = auth

    def get(self, title: str, **kwargs) -> Optional[Tiddler]:
        """Get a tiddler based on its title.

        Example

        >>> twclient = tiddlywiki.twclient.TWClient(tw_url, auth=auth)
        >>> tiddler = tw_client.get("$:/DefaultTiddlers")
        """

        # If the filter is None then use the default search filter.
        if title is None:
            raise NotImplementedError("Title must be provided.")
        path = f"recipes/default/tiddlers/{quote(title)}"
        logger.info("Get tiddler: %s", path)
        try:
            response = self.session.get(path, auth=self._auth, **kwargs)
        except ConnectionError as error:
            raise TWError(
                f"Connection error to {self.session.prefix_url} {path} raised error: {error}"
            ) from error

        # If the status_code is 200 the search worked and the result need to be process
        # Otherwise just report the error response
        # $:/core/modules/server/routes/get-tiddler.js
        tid: Optional[Tiddler]
        if response.status_code == 200:
            tid_dict = response.json()
            tid = Tiddler.from_dict(tid_dict)
        elif response.status_code == 401:
            error_msg = "Unauthorised"
            logger.error(error_msg)
            raise TWError(error_msg)
        elif response.status_code == 404:
            logger.warning(
                "Tiddler titled '%s' not found code: %s URL: %s",
                title,
                response.status_code,
                self.session.last_full_url,
            )
            tid = None
        else:
            error_msg = (
                f"Tiddler titled '{title!r}' not found "
                f"code: {response.status_code!r} "
                f"Response: {response.content!r}, "
                f"Headers: {response.request.headers!r}, "
                f"Body: {response.request.body!r}"
            )
            # logger.error(error_msg)
            raise TWError(f"Failed {error_msg}")
        return tid

    def search(self, filter: str, exclude: str = None, **kwargs) -> List[Tiddler]:
        """Find tiddlers based on a filter.

        Example

        >>> twclient = tiddlywiki.twclient.TWClient(tw_url, auth=auth)
        >>> # BY default gets all field except the main text field
        >>> tiddlers = tw_client.search(filter="[all[tiddlers]!is[system]sort[title]]")
        >>> # Get all tiddler fields
        >>> tiddlers = tw_client.search(filter="[all[tiddlers]!is[system]sort[title]]", exclude="all")
        """

        # If the filter is None then use the default search filter.
        params = {"filter": filter}
        if filter is None:
            params["filter"] = "[all[tiddlers]!is[system]sort[title]]"
        if exclude is not None:
            params["exclude"] = exclude
        quoted_params = urlencode(
            params, safe="()", quote_via=quote
        )  # quote(search_filter)
        path = f"recipes/default/tiddlers.json?{quoted_params}"
        logger.debug(
            "Search tiddler based on params: %s and final url: %s", params, path
        )
        try:
            response = self.session.get(
                path,
                auth=self._auth,
                headers={
                    "content-type": "application/json",
                    "X-Requested-With": "TiddlyWiki",
                },
                **kwargs,
            )
        except ConnectionError as error:
            raise TWError(
                f"Connection error to {self.session.last_full_url} raised error: {error}"
            ) from error

        # If the status_code is 200 the search worked and the result need to be process
        # Otherwise just report the error response
        # $:/core/modules/server/routes/put-tiddler.js
        if response.status_code == 200:
            return [Tiddler.from_dict(obj) for obj in response.json()]
        elif response.status_code == 401:
            error_msg = "Unauthorised"
            # logger.error(error_msg)
            raise TWError(error_msg)
        elif response.status_code == 404:
            error_msg = f"Search with params '{params}' page not found URL: {self.session.last_full_url}"
            logger.warning(error_msg)
            raise TWError(error_msg)
        else:
            error_msg = (
                f"Tiddler search {quoted_params!r} API request failed\n"
                f"Url: {response.url}\n"
                f"code: {response.status_code!r}\n"
                f"Response: {response.content!r}\n"
                f"Headers: {response.request.headers!r}\n"
                f"Body: {response.request.body!r}"
            )
            # logger.error(error_msg)
            raise TWError(f"Failed {error_msg}")

    def install_all_external_filters(self):
        """Installs a Tiddler to add allow all external filters.

        This adds the following tiddler defined as `allow_external_tiddler`
        """
        allow_external_tiddler = Tiddler("$:/config/Server/AllowAllExternalFilters", text="yes")
            
        result = self.save(allow_external_tiddler)
        return result

    def install_filter_route(self):
        """Installs a Tiddler to add a new route to a Tiddlywiki node.js server.

        This adds the following tiddler defined as `filter_tiddler`
        """
        filter_tiddler = Tiddler(
            "$:/core/modules/server/routes/get-filter-json.js",
            type="application/javascript",
            text=textwrap.dedent("""
                    /*\\
                    title: $:/core/modules/server/routes/get-filter-json.js
                    type: application/javascript
                    module-type: route

                    GET /recipes/default/filter.json?filter=<filter>

                    As used by the tiddlywiki-python Library to use the filter method

                    This allow the tiddlywiki instance to run tiddlywiki filter and return the result based on the tiddler loaded.

                    Discussion on use:

                    I feel this lends to very flexible distributed processing based on tiddly nodes.
                    This route is still held being the auth to protect it some what.
                    Also it is also subject to the "$:/config/Server/AllowAllExternalFilters"
                    being set to 'yes' So the filter available can be restricted.

                    This is only like the tiddler search route "$:/core/modules/server/routes/get-tiddlers-json.js"
                    except it doesn't return all the tiddler information by the default.
                    This allow more intelligent processing to happen and filter result to be returned.

                    \\*/
                    (function() {

                        /*jslint node: true, browser: true */
                        /*global $tw: false */
                        "use strict";
                        
                        var DEFAULT_FILTER = "[all[tiddlers]!is[system]sort[title]]";
                        
                        exports.method = "GET";
                        
                        exports.path = /^\/recipes\/default\/filter.json$/;
                        
                        exports.handler = function(request,response,state) {
                            var filter = state.queryParameters.filter || DEFAULT_FILTER;
                            if(state.wiki.getTiddlerText("$:/config/Server/AllowAllExternalFilters") !== "yes") {
                                if(state.wiki.getTiddlerText("$:/config/Server/ExternalFilters/" + filter) !== "yes") {
                                    console.log("Blocked attempt to GET /recipes/default/filter.json with filter: " + filter);
                                    response.writeHead(403);
                                    response.end();
                                    return;
                                }
                            }
                            var filter_results = state.wiki.filterTiddlers(filter);
                            var text = JSON.stringify(filter_results);
                            state.sendResponse(200,{"Content-Type": "application/json"},text,"utf8");
                        };
                        
                        }());
    
            """)
        )
        result = self.save(filter_tiddler)
        return result

    def filter(self, filter: str, **kwargs) -> List[str]:
        """Find result list based on any filter (Not standard Tiddlywiki route!! See help text)

        Example

        >>> twclient = tiddlywiki.twclient.TWClient(tw_url, auth=auth)
        >>> # Run Filter on the service to get all titles
        >>> tiddler_name = tw_client.filter(filter="[all[tiddlers]!is[system]sort[title]]")
        >>> # Run a filter that does a calculation
        >>> tiddlers = tw_client.filter(filter="[[2]add[2]]")
        >>> # Run a filter that does a calculation
        >>> tiddlers = tw_client.filter(filter="=3 =5 +[add[2]]")
        >>> # Run a filter that does a calculation with strings returns list with three strings
        >>> tiddlers = tw_client.filter(filter="[[test]] [[foo]] [[bar]] +[addprefix[test_]]")

        Requires tha following Tiddler installed on the tiddlyiwki node.js server.
        The tiddler required is documented and able to be run/installed by running:

        ```
        install_filter_route()
        ```

        Then the node.js server needs to be restarted.

        To allow any filter to be run it also need another tiddler:

        ```
        install_all_external_filters()
        ```

        """

        # If the filter is None then use the default search filter.
        params = {"filter": filter}
        if filter is None:
            params["filter"] = "[all[tiddlers]!is[system]sort[title]]"
        quoted_params = urlencode(
            params, safe="()", quote_via=quote
        )  # quote(search_filter)
        path = f"recipes/default/filter.json?{quoted_params}"
        logger.info("Search tiddler: %s", path)
        try:
            response = self.session.get(
                path,
                auth=self._auth,
                headers={
                    "content-type": "application/json",
                    "X-Requested-With": "TiddlyWiki",
                },
                **kwargs,
            )
        except ConnectionError as error:
            raise TWError(
                f"Connection error to {self.session.prefix_url} {path} raised error: {error}"
            ) from error

        # If the status_code is 200 the search worked and the result need to be processed
        # Otherwise just report the error response
        # $:/core/modules/server/routes/put-tiddler.js
        if response.status_code == 200:
            return list(response.json())
        elif response.status_code == 401:
            error_msg = "Unauthorised"
            # logger.error(error_msg)
            raise TWError(error_msg)
        elif response.status_code == 404:
            error_msg = "Route not found requires a filter route in the server."
            # logger.error(error_msg)
            raise TWError(error_msg)
        else:
            error_msg = (
                f"Tiddler search {quoted_params!r} API request failed\n"
                f"Url: {response.url}\n"
                f"code: {response.status_code!r}\n"
                f"Response: {response.content!r}\n"
                f"Headers: {response.request.headers!r}\n"
                f"Body: {response.request.body!r}"
            )
            # logger.error(error_msg)
            raise TWError(f"Failed {error_msg}")

    def save(self, tiddler: Tiddler, **kwargs) -> SaveReceipt:
        """Save a tiddler based on its title.
        
        This requries a new tiddler.
        A tiddler example is in useful_tiddlers.py script.
        """

        # If the filter is None then use the default search filter.
        if tiddler is None:
            raise NotImplementedError("A Tiddler must be provided.")
        path = f"recipes/default/tiddlers/{quote(tiddler.title)}"
        logger.info("Saving tiddler: %s", path)
        try:
            response = self.session.put(
                path,
                json=tiddler.to_tw_dict(),
                headers={
                    "content-type": "application/json",
                    "X-Requested-With": "TiddlyWiki",
                },
                auth=self._auth,
                **kwargs,
            )
        except ConnectionError as error:
            raise TWError(
                f"Connection error to {self.session.prefix_url} {path} raised error: {error}"
            ) from error

        # If the status_code is 204 the search worked and the result need to be process
        # Otherwise just report the error response
        if response.status_code == 204:
            logger.debug(
                "Sucessfully Saved tiddler\ncontent:\n%s\nbody:\n%s\nheaders:\n%s",
                response.content,
                response.request.body,
                response.headers,
            )
            etag = response.headers["Etag"]
            return SaveReceipt(status=True, etag=etag, tiddler=tiddler)
        elif response.status_code == 401:
            error_msg = "Unauthorised"
            # logger.error(error_msg)
            raise TWError(error_msg)
        else:
            error_msg = (
                f"Tiddler titled '{tiddler.title!r}' not saved "
                f"code: {response.status_code!r} "
                f"Response: {response.content!r}, "
                f"Headers: {response.request.headers!r}, "
                f"Body: {response.request.body!r}"
            )
            # logger.error(error_msg)
            raise TWError(f"Failed {error_msg}")

    def delete(self, title: str, **kwargs) -> DeleteReceipt:
        """get a tiddler based on its title."""

        # If the filter is None then use the default search filter.
        if title is None:
            raise NotImplementedError("Title must be provided.")
        path = f"bags/default/tiddlers/{quote(title)}"
        logger.info("Delete tiddler: %s", path)
        try:
            response = self.session.delete(
                path,
                headers={
                    "content-type": "application/json",
                    "X-Requested-With": "TiddlyWiki",
                },
                auth=self._auth,
                **kwargs,
            )
        except ConnectionError as error:
            raise TWError(
                f"Connection error to {self.session.prefix_url} {path} raised error: {error}"
            ) from error

        # If the status_code is 200 the search worked and the result need to be process
        # Otherwise just report the error response
        # Right now Tiddly wiki server will only respond with 204 based on
        # $:/core/modules/server/routes/delete-tiddler.js
        if response.status_code == 204:
            return DeleteReceipt(True, title)
        elif response.status_code == 401:
            error_msg = "Unauthorised"
            # logger.error(error_msg)
            raise TWError(error_msg)
        else:
            error_msg = (
                f"Tiddler titled '{title!r}' not deleted "
                f"code: {response.status_code!r} "
                f"Response: {response.content!r}, "
                f"Headers: {response.request.headers!r}, "
                f"Body: {response.request.body!r}"
            )
            # logger.error(error_msg)
            raise TWError(f"Failed {error_msg}")
