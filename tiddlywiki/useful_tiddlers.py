"""
Library to hold useful tiddlers.

Author: Robert Munnoch
"""
# pylint: disable=anomalous-backslash-in-string


from textwrap import dedent

from .tw import Tiddler

filterroute = Tiddler(
    "$:/core/modules/server/routes/get-filter-json.js",
    type="application/javascript",
    fields={"module-type": "route"},
    text=dedent(
        """
    /*
    title: $:/core/modules/server/routes/get-filter-json.js
    type: application/javascript
    module-type: route

    GET /recipes/default/filter.json?filter=<filter>

    \\*/
    (function() {

    /*jslint node: true, browser: true */
    /*global $tw: false */
    "use strict";

    var DEFAULT_FILTER = "[all[tiddlers]!is[system]sort[title]]";

    exports.method = "GET";

    exports.path = /^\\/recipes\\/default\\/filter.json$/;

    exports.handler = function(request,response,state) {
        var filter = state.queryParameters.filter || DEFAULT_FILTER;
        var titles = state.wiki.filterTiddlers(filter);
        var text = JSON.stringify(titles);
        state.sendResponse(200,{"Content-Type": "application/json"},text,"utf8");
    };

    }());
    """
    ),
)
